package com.freecharge.hackathon.test;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.MapFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.hackathon.feign.FeignConfiguration;
import com.freecharge.hackathon.feign.FeignServiceOperations;
import com.freecharge.hackathon.services.FeignServiceClient;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FeignConfiguration.class)
@TestPropertySource("classpath:test.properties")
public class SplitwiseApiTest {

	@Autowired
	private MapFactoryBean splitwiseApiClientsRegistry;

	private String accessToken = "c2hhc2hhbmttaXR0YWwwMjJAZ21haWwuY29tOlNtaXR0YWwxMjA5QA==";

	@Test
	public void testCurrentUserFriends() throws Exception {
		FeignServiceClient client = getClient(FeignServiceOperations.GET_CURRENT_USER_FRIENDS);
		Object friends = client.getFriends(accessToken);
		System.out.println(friends);
	}

	//@Test
	public void testCurrentUserGroups() throws Exception {
		FeignServiceClient client = getClient(FeignServiceOperations.GET_USER_GROUPS);
		Object friends = client.getGroups(accessToken);
		System.out.println(friends);
	}

	private FeignServiceClient getClient(FeignServiceOperations mandateServiceOperation) throws Exception {
		Map<FeignServiceOperations, FeignServiceClient> map = (Map<FeignServiceOperations, FeignServiceClient>) (Map<?, ?>) splitwiseApiClientsRegistry
				.getObject();
		FeignServiceClient mandateServiceClient = map.get(mandateServiceOperation);
		return mandateServiceClient;
	}

}
