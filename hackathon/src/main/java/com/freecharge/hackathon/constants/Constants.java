package com.freecharge.hackathon.constants;

public class Constants {

	public static final String DATABASE_ENTITY_SCAN_PACKAGES = "com.doctordesk.spring.database.entities";

	public static final String DATASOURCE = "dataSource";

	public static final String BCRYPT_PASSWORD_ENCODER = "bCryptPasswordEncoder";

	public static final String DEFAULT = "default";

	public static final String SUCCESS = "Success";

	public static final String ADD = "ADD";

	public static final String USER_MESSAGES_PROPERTY_FILE = "classpath:UserMessages.properties";

	public static final int LENGTH_8 = 8;

	public static final int LENGTH_16 = 16;

	public static final int LENGTH_1 = 1;

	public static final int LENGTH_3 = 3;

	public static final int LENGTH_30 = 30;

	public static final int LENGTH_5 = 5;

	public static final int LENGTH_10 = 10;

	public static final int LENGTH_15 = 15;

	public static final int LENGTH_0 = 0;

	public static final int LENGTH_35 = 35;

	public static final int LENGTH_50 = 50;

	public static final int LENGTH_100 = 100;

	public static final String CROSS_ORIGIN_LOCAL = "http://localhost:3000";

	public static final int KEEP_ALIVE_DURATION = 25 * 60 * 1000;
}
