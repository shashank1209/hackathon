package com.freecharge.hackathon.constants;

public abstract class RestUrlConstants {

	public static final String REST_V1 = "/rest/freecharge/v1/";

	public static final String PING = "/ping";
	
	public static final String FRIENDS = "/friends";

	public static final String GROUPS = "/groups";
	
	public static final String USER = "/user";

}
