package com.freecharge.hackathon.constants;

public class ValidationMessagesConstants {

	public static final String REQUESTID_LENGTH = "{requestId.length}";

	public static final String TOKEN_INVALID = "{token.invalid}";

	public static final String GENERIC_FIELD_LENGTH = "{generic.field.length}";

	public static final String NOT_NULL_OR_BLANK = "{not.null.or.blank}";

}
