package com.freecharge.hackathon.feign;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
public enum FeignServiceOperations {

	GET_CURRENT_USER_FRIENDS,
	GET_USER_GROUPS;
}
