package com.freecharge.hackathon.feign;

import com.freecharge.hackathon.models.downstream.SplitwiseResponseBody;

import lombok.Getter;

@Getter
public class ClientConfig {
	private int connectTimeoutMillis = 3000;

	private int readTimeoutMillis = 10000;

	private String baseUrl;

	private boolean isCustomLoggingEnabled = true;

	private boolean isRequestLogEnabled = true;

	private boolean isResponseLogEnabled = true;

	private Class<? extends SplitwiseResponseBody> responseType = SplitwiseResponseBody.class;

	public static ClientConfig getInstance() {
		return new ClientConfig();
	}

	public ClientConfig baseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
		return this;
	}

	public ClientConfig readTimeoutMillis(int readTimeoutMillis) {
		this.readTimeoutMillis = readTimeoutMillis;
		return this;
	}

	public ClientConfig connectTimeoutMillis(int connectTimeoutMillis) {
		this.connectTimeoutMillis = connectTimeoutMillis;
		return this;
	}

	ClientConfig isCustomLoggingEnabled(boolean isCustomLoggingEnabled) {
		this.isCustomLoggingEnabled = isCustomLoggingEnabled;
		return this;
	}

	ClientConfig isRequestLogEnabled(boolean isRequestLogEnabled) {
		this.isRequestLogEnabled = isRequestLogEnabled;
		return this;
	}

	ClientConfig isResponseLogEnabled(boolean isResponseLogEnabled) {
		this.isResponseLogEnabled = isResponseLogEnabled;
		return this;
	}

	ClientConfig responseType(Class<? extends SplitwiseResponseBody> responseType) {
		this.responseType = responseType;
		return this;
	}
}