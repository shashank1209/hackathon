package com.freecharge.hackathon.feign;

import java.io.IOException;
import java.lang.reflect.Type;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.hackathon.models.downstream.SplitwiseResponseBody;

import feign.Response;
import feign.jackson.JacksonDecoder;

public class SplitwiseResponseJacksonDecoder<T extends SplitwiseResponseBody> extends JacksonDecoder {

	protected Class<T> responseType;

	public SplitwiseResponseJacksonDecoder() {
		responseType = (Class<T>) SplitwiseResponseBody.class;
	}

	public SplitwiseResponseJacksonDecoder(Class<T> responseType) {
		super(new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false));
		this.responseType = responseType;
	}

	public SplitwiseResponseJacksonDecoder(ObjectMapper mapper) {
		super(mapper);
	}

	@Override
	public Object decode(Response response, Type type) throws IOException {
		T axisResponseBody = (T) super.decode(response, responseType);
		return axisResponseBody;
	}
}
