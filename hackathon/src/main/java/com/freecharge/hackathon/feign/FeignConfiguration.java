package com.freecharge.hackathon.feign;

import java.util.HashMap;
import java.util.Map;

import com.freecharge.hackathon.models.downstream.SplitwiseGroupResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.MapFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.hackathon.models.downstream.SplitwiseFriendsResponseBody;
import com.freecharge.hackathon.services.FeignServiceClient;

import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Configuration
public class FeignConfiguration {

	@Value("${splitwise.api.base.url}")
	protected String baseUrl;

	@Bean("splitwiseApiClientsRegistry")
	public MapFactoryBean splitwiseApiClientsRegistry() throws Exception {

		Map<FeignServiceOperations, FeignServiceClient> mandateClientRegistry = new HashMap<>();

		mandateClientRegistry.put(FeignServiceOperations.GET_CURRENT_USER_FRIENDS, buildClient(getClientConfig(FeignServiceOperations.GET_CURRENT_USER_FRIENDS)));
		mandateClientRegistry.put(FeignServiceOperations.GET_USER_GROUPS, buildClient(getClientConfig(FeignServiceOperations.GET_USER_GROUPS)));

		MapFactoryBean mapFactoryBean = new MapFactoryBean();
		mapFactoryBean.setSourceMap(mandateClientRegistry);
		return mapFactoryBean;
	}

	private ClientConfig getClientConfig(FeignServiceOperations type) {

		switch (type) {
			case GET_CURRENT_USER_FRIENDS:
				return ClientConfig.getInstance().baseUrl(baseUrl)
						.responseType(SplitwiseFriendsResponseBody.class);
			case GET_USER_GROUPS:
				return ClientConfig.getInstance().baseUrl(baseUrl)
						.responseType(SplitwiseGroupResponseBody.class);
			default:
				return null;
		}
	}

	private Feign.Builder configureBuilder(ClientConfig clientConfig) {
		return Feign.builder()
				.decoder(new JacksonDecoder(
						new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)))
				.encoder(new JacksonEncoder(new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL)))
				.options(new Request.Options(clientConfig.getConnectTimeoutMillis(),
						clientConfig.getReadTimeoutMillis()))
				.logLevel(Logger.Level.FULL);
	}

	private FeignServiceClient buildClient(ClientConfig clientConfig) {
		return configureBuilder(clientConfig)
				.decoder(new SplitwiseResponseJacksonDecoder(clientConfig.getResponseType()))
				.target(FeignServiceClient.class, clientConfig.getBaseUrl());
	}

}
