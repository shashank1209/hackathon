package com.freecharge.hackathon.exceptions;

public interface IResponseError {

	public String getErrorCode();

	public String getStatus();

	public String getUserMessage();

	public String getDevMessage();

	public String getMoreInfo();
}
