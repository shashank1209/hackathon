package com.freecharge.hackathon.exceptions;

public class ApplicationError extends AbstractApplicationError {

	private static final long serialVersionUID = 1L;

	public ApplicationError(ExceptionEnum error, String userMessage, String devMessage, String moreInfo) {
		super(error, userMessage, devMessage, moreInfo);
	}

	public ApplicationError(ExceptionEnum error) {
		super(error, null, null, null);
	}
}
