package com.freecharge.hackathon.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_EMPTY)
public class AbstractResponseError implements IResponseError {

	protected String errorCode;

	protected String status;

	protected String userMessage;

	protected String devMessage;

	protected String moreInfo;
}
