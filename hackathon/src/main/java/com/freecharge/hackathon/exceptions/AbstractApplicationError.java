package com.freecharge.hackathon.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_EMPTY)
public abstract class AbstractApplicationError extends RuntimeException implements IApplicationError {

	private static final long serialVersionUID = 1L;

	public AbstractApplicationError(ExceptionEnum error, String userMessage, String devMessage, String moreInfo) {
		super(null, null, false, false);
		this.error = error;
		this.userMessage = userMessage;
		this.devMessage = devMessage;
		this.moreInfo = moreInfo;
	}

	protected ExceptionEnum error;

	protected String userMessage;

	protected String devMessage;

	protected String moreInfo;
}
