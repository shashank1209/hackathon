package com.freecharge.hackathon.exceptions;

import org.springframework.http.HttpStatus;

public enum ExceptionEnum {
	INTERNAL_SERVER_ERROR("500", "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR), NOT_FOUND("404",
			"NOT_FOUND", HttpStatus.NOT_FOUND), VALIDATION_FAILED("ER-100", "VALIDATION_FAILED",
					HttpStatus.BAD_REQUEST), USER_NOT_FOUND("ER-101", "AUTHENTICATION_FAILED",
							HttpStatus.UNAUTHORIZED), INVALID_TOKEN("ER-102", "INVALID_TOKEN",
									HttpStatus.UNAUTHORIZED), DOWNSTREAM_TIMEOUT("ER-103", "DOWNSTREAM_TIMEOUT",
											HttpStatus.UNAUTHORIZED),;

	private String errorCode;

	private String status;

	private HttpStatus httpStatus;

	private ExceptionEnum(String errorCode, String status, HttpStatus httpStatus) {
		this.errorCode = errorCode;
		this.status = status;
		this.httpStatus = httpStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
