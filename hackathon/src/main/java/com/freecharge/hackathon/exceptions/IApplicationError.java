package com.freecharge.hackathon.exceptions;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 25, 2019
 */
public interface IApplicationError {

	public ExceptionEnum getError();

	public String getUserMessage();

	public String getDevMessage();

	public String getMoreInfo();
}
