package com.freecharge.hackathon.adapters;

import java.util.Map;

import com.freecharge.hackathon.models.downstream.SplitwiseGroupResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.MapFactoryBean;
import org.springframework.stereotype.Service;

import com.freecharge.hackathon.feign.FeignServiceOperations;
import com.freecharge.hackathon.models.downstream.SplitwiseFriendsResponseBody;
import com.freecharge.hackathon.services.FeignServiceClient;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Service
public class IntegrationAdapter implements IIntegrationAdapter {

	@Autowired
	private MapFactoryBean splitwiseApiClientsRegistry;

	@Override
	public SplitwiseFriendsResponseBody getUserFriends(String accessToken) {
		SplitwiseFriendsResponseBody friends = null;
		try {
			FeignServiceClient client = getClient(FeignServiceOperations.GET_CURRENT_USER_FRIENDS);
			friends = client.getFriends(accessToken);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return friends;
	}

	@Override
	public SplitwiseGroupResponseBody getUserGroups(String accessToken) {
		try {
			FeignServiceClient client = getClient(FeignServiceOperations.GET_USER_GROUPS);
			return client.getGroups(accessToken);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private FeignServiceClient getClient(FeignServiceOperations mandateServiceOperation) throws Exception {
		Map<FeignServiceOperations, FeignServiceClient> map = (Map<FeignServiceOperations, FeignServiceClient>) (Map<?, ?>) splitwiseApiClientsRegistry
				.getObject();
		FeignServiceClient mandateServiceClient = map.get(mandateServiceOperation);
		return mandateServiceClient;
	}

}
