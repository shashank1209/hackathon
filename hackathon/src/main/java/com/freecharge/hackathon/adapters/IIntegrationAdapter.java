package com.freecharge.hackathon.adapters;

import com.freecharge.hackathon.models.downstream.SplitwiseFriendsResponseBody;
import com.freecharge.hackathon.models.downstream.SplitwiseGroupResponseBody;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
public interface IIntegrationAdapter {
	SplitwiseFriendsResponseBody getUserFriends(String accessToken);

	SplitwiseGroupResponseBody getUserGroups(String accessToken);
}
