package com.freecharge.hackathon.services;

import com.freecharge.hackathon.models.responses.GetUserFriendsResponse;
import com.freecharge.hackathon.models.responses.GetUserGroupResponse;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
public interface IUserService {
	GetUserFriendsResponse getUserFriends(String accessToken);

	GetUserGroupResponse getGroupFriends(String accessToken);

}
