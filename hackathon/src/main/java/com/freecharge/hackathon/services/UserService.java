package com.freecharge.hackathon.services;

import com.freecharge.hackathon.models.downstream.SplitwiseGroupResponseBody;
import com.freecharge.hackathon.models.responses.GetUserGroupResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.hackathon.adapters.IIntegrationAdapter;
import com.freecharge.hackathon.converters.Converter;
import com.freecharge.hackathon.models.downstream.SplitwiseFriendsResponseBody;
import com.freecharge.hackathon.models.responses.GetUserFriendsResponse;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Service
public class UserService implements IUserService {

	@Autowired
	IIntegrationAdapter integrationAdapter;

	@Override
	public GetUserFriendsResponse getUserFriends(String accessToken) {
		SplitwiseFriendsResponseBody userFriends = integrationAdapter.getUserFriends(accessToken);
		return Converter.toGetUserFriendsResponse(userFriends);
	}

	@Override
	public GetUserGroupResponse getGroupFriends(String accessToken) {
		SplitwiseGroupResponseBody userGroupFriends = integrationAdapter.getUserGroups(accessToken);
		GetUserGroupResponse getUserGroupResponse = new GetUserGroupResponse();
		BeanUtils.copyProperties(userGroupFriends, getUserGroupResponse);
		return getUserGroupResponse;
	}
}
