package com.freecharge.hackathon.services;

import com.freecharge.hackathon.models.downstream.SplitwiseFriendsResponseBody;

import com.freecharge.hackathon.models.downstream.SplitwiseGroupResponseBody;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.springframework.web.bind.annotation.PathVariable;

@Headers({ "Content-Type:application/json" })
public interface FeignServiceClient {

	@RequestLine("GET /get_friends")
	@Headers("Authorization: Basic {accessToken}")
	SplitwiseFriendsResponseBody getFriends(@Param("accessToken") String accessToken);

	@RequestLine("GET /get_groups")
	SplitwiseGroupResponseBody getGroups(@Param("accessToken")String accessToken);
}
