package com.freecharge.hackathon.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.freecharge.hackathon.models.beans.Balance;
import com.freecharge.hackathon.models.beans.CurrencyCodesEnum;
import com.freecharge.hackathon.models.beans.Friend;
import com.freecharge.hackathon.models.downstream.SplitwiseBalanceDto;
import com.freecharge.hackathon.models.downstream.SplitwiseFriendDto;
import com.freecharge.hackathon.models.downstream.SplitwiseFriendsResponseBody;
import com.freecharge.hackathon.models.responses.GetUserFriendsResponse;

public class Converter {

	public static GetUserFriendsResponse toGetUserFriendsResponse(SplitwiseFriendsResponseBody input) {
		GetUserFriendsResponse output = new GetUserFriendsResponse();
		if (input.getFriends() != null) {
			List<Friend> items = new ArrayList<Friend>();
			for (SplitwiseFriendDto friend : input.getFriends()) {
				if (friend != null) {
					Friend friend2 = toFriend(friend);
					items.add(friend2);
				}
			}
			output.setItems(items);
			output.setCount(items.size());
		}
		return output;
	}

	public static Friend toFriend(SplitwiseFriendDto input) {
		Friend output = null;
		if (input != null) {
			output = new Friend();
			BeanUtils.copyProperties(input, output);
			output.setGrossBalance(toBalance(input.getBalance()));
		}
		return output;
	}

	public static Balance toBalance(List<SplitwiseBalanceDto> input) {
		Balance output = new Balance();
		output.setCurrencyCode(CurrencyCodesEnum.INR);
		if (input != null) {
			for (SplitwiseBalanceDto balance : input) {
				if (balance != null) {
					Double bal = Double.valueOf(balance.getAmount());
					if (CurrencyCodesEnum.INR.name().equals(balance.getCurrencyCode())) {
						output.setAmount(output.getAmount() + bal);
					}
				}

			}
		}
		return output;
	}
}
