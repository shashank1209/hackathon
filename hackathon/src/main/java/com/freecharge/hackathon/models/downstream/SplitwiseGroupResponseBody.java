package com.freecharge.hackathon.models.downstream;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SplitwiseGroupResponseBody extends SplitwiseResponseBody{
    private List<SplitwiseGroupDto> groups;
}
