package com.freecharge.hackathon.models.downstream;

import java.util.List;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
public class SplitwiseResponseBody {

	private List<SplitwiseFriendDto> friends;
}
