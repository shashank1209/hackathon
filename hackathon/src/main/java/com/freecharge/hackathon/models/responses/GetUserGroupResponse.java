package com.freecharge.hackathon.models.responses;

import com.freecharge.hackathon.models.beans.DebtsInfo;
import com.freecharge.hackathon.models.downstream.SplitwiseFriendDto;
import lombok.Data;

import java.util.List;

@Data
public class GetUserGroupResponse {
    private String name;
    private String id;
    private List<SplitwiseFriendDto> members;
    private List<DebtsInfo> original_debts;
    private String whiteboard;
    private String group_type;
    private String invite_link;
}
