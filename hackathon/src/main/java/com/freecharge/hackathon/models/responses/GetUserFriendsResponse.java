package com.freecharge.hackathon.models.responses;

import java.util.List;

import com.freecharge.hackathon.models.beans.Friend;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
public class GetUserFriendsResponse {

	private int count;
	private List<Friend> items;
}
