package com.freecharge.hackathon.models.downstream;

import com.freecharge.hackathon.models.beans.DebtsInfo;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SplitwiseGroupDto {
    private String id;
    private String name;
    private Date updated_at;
    private List<SplitwiseFriendDto> members;
    private boolean simplify_by_default;
    private List<DebtsInfo> original_debts;
    private List<DebtsInfo> simplified_debts;
    private String whiteboard;
    private String group_type;
    private String invite_link;
}
