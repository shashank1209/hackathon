package com.freecharge.hackathon.models.downstream;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
public class SplitwiseBalanceDto {
	@JsonProperty("currency_code")
	private String currencyCode;
	private String amount;
}