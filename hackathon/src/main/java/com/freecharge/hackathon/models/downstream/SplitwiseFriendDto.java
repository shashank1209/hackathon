package com.freecharge.hackathon.models.downstream;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SplitwiseFriendDto {
	private int id;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("last_name")
	private String lastName;

	private String email;

	@JsonProperty("registration_status")
	private String registrationStatus;

	private SplitwisePictureDto picture;

	private List<SplitwiseBalanceDto> balance;

	@JsonProperty("updated_at")
	private String updatedAt;

}
