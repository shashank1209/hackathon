package com.freecharge.hackathon.models.requests;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.freecharge.hackathon.constants.ValidationMessagesConstants;

import lombok.Data;

@Data
public class GenericRequest<T> {

	@NotBlank
	@Size(min = 10, max = 30, message = ValidationMessagesConstants.REQUESTID_LENGTH)
	private String requestId;

	@Valid
	private T request;
}
