package com.freecharge.hackathon.models.beans;

import lombok.Data;

@Data
public class DebtsInfo {
    private String from;
    private String to;
    private Double amount;
    private String currency_code;
}
