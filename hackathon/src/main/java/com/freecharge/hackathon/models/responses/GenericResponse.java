package com.freecharge.hackathon.models.responses;

import com.freecharge.hackathon.exceptions.IResponseError;

import lombok.Data;

@Data
public class GenericResponse<T> {

	private int code;

	private String message;

	private IResponseError errors;

	private T response;

	public GenericResponse() {
		super();
	}

	public GenericResponse(int code, String message, String requestId, T response) {
		super();
		this.code = code;
		this.message = message;
		this.response = response;
	}

	public GenericResponse(int code, String message, String requestId, IResponseError errors, T response) {
		super();
		this.code = code;
		this.message = message;
		this.errors = errors;
		this.response = response;
	}
}
