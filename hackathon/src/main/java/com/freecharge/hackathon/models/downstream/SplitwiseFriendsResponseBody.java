package com.freecharge.hackathon.models.downstream;

import java.util.List;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
public class SplitwiseFriendsResponseBody extends SplitwiseResponseBody {

	private List<SplitwiseFriendDto> friends;
}
