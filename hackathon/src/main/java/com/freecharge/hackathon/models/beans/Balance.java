package com.freecharge.hackathon.models.beans;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
public class Balance {
	private CurrencyCodesEnum currencyCode;
	private double amount;
}