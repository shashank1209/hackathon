package com.freecharge.hackathon.models.downstream;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
public class SplitwisePictureDto {
	private String small;

	private String medium;

	private String large;

}