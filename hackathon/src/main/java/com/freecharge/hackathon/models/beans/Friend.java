package com.freecharge.hackathon.models.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.freecharge.hackathon.models.downstream.SplitwisePictureDto;

import lombok.Data;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 26, 2019
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Friend {
	private int id;

	private String firstName;

	private String lastName;

	private String email;

	private String registrationStatus;

	private SplitwisePictureDto picture;

	private Balance grossBalance;

	private String updatedAt;

}
