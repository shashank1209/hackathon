package com.freecharge.hackathon.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.freecharge.hackathon.exceptions.AbstractApplicationError;
import com.freecharge.hackathon.exceptions.ResponseError;
import com.freecharge.hackathon.handlers.IApplicationErrorHandler;
import com.freecharge.hackathon.models.responses.GenericResponse;
import com.freecharge.hackathon.properties.UserMessageProperties;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 25, 2019
 */
@RestController
@ControllerAdvice
public class AbstractController {

	@Autowired
	private IApplicationErrorHandler applicationErrorHandler;

	@Autowired
	private UserMessageProperties userMessageProperties;

	@ExceptionHandler({ Exception.class })
	public GenericResponse<Object> handleException(Exception e, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse) {
		GenericResponse<Object> response = new GenericResponse<Object>();
		response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		response.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		ResponseError responseError = new ResponseError();
		responseError.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
		responseError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		responseError.setUserMessage(
				userMessageProperties.getUserMessage(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value())));
		response.setErrors(responseError);
		return response;
	}

	@ExceptionHandler({ MethodArgumentNotValidException.class })
	public GenericResponse<Object> handleValidationException(MethodArgumentNotValidException e,
			HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
		GenericResponse<Object> response = new GenericResponse<Object>();
		response.setCode(HttpStatus.BAD_REQUEST.value());
		response.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
		ResponseError responseError = new ResponseError();
		responseError.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
		responseError.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
		responseError.setUserMessage(e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
		response.setErrors(responseError);
		return response;
	}

	@ExceptionHandler({ RuntimeException.class })
	public GenericResponse<Object> handleRuntimeException(RuntimeException e, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse) {
		e.printStackTrace();
		GenericResponse<Object> response = new GenericResponse<Object>();
		response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		response.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		ResponseError responseError = new ResponseError();
		responseError.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
		responseError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		responseError.setUserMessage(
				userMessageProperties.getUserMessage(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value())));
		response.setErrors(responseError);
		return response;
	}

	@ExceptionHandler({ AbstractApplicationError.class })
	public GenericResponse<Object> handleApplicationException(AbstractApplicationError e,
			HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
		GenericResponse<Object> response = applicationErrorHandler.handleApplicationError(e);
		return response;
	}
}
