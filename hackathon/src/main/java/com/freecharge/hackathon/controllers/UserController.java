package com.freecharge.hackathon.controllers;

import com.freecharge.hackathon.models.responses.GetUserGroupResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.freecharge.hackathon.constants.RestUrlConstants;
import com.freecharge.hackathon.models.responses.GenericResponse;
import com.freecharge.hackathon.models.responses.GetUserFriendsResponse;
import com.freecharge.hackathon.services.IUserService;

/**
 * @author shashank.mittal Shashank Mittal
 * @created Jun 25, 2019
 */
@RestController
@RequestMapping(value = RestUrlConstants.REST_V1 + RestUrlConstants.USER)
public class UserController {

	@Autowired
	IUserService userService;

	@RequestMapping(value = { RestUrlConstants.PING }, method = { RequestMethod.GET })
	public GenericResponse<String> ping() {
		GenericResponse<String> response = new GenericResponse<String>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage("SUCCESS");
		response.setResponse("PONG");
		return response;
	}

	@RequestMapping(value = { RestUrlConstants.FRIENDS }, method = { RequestMethod.GET })
	public GenericResponse<GetUserFriendsResponse> getUserFriends(@RequestHeader("accessToken") String accessToken) {

		GetUserFriendsResponse friendsResponse = userService.getUserFriends(accessToken);
		GenericResponse<GetUserFriendsResponse> response = new GenericResponse<GetUserFriendsResponse>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage("SUCCESS");
		response.setResponse(friendsResponse);
		return response;
	}

	@RequestMapping(value = RestUrlConstants.GROUPS, method = RequestMethod.GET)
	public GenericResponse<GetUserGroupResponse> getUserGroups(@RequestHeader("accessToken")String accessToken){
		GetUserGroupResponse getUserGroupResponse = userService.getGroupFriends(accessToken);
		GenericResponse<GetUserGroupResponse> response = new GenericResponse<>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage("SUCCESS");
		response.setResponse(getUserGroupResponse);
		return response;
	}
}
