package com.freecharge.hackathon.handlers;

import com.freecharge.hackathon.exceptions.IApplicationError;
import com.freecharge.hackathon.models.responses.GenericResponse;

public interface IApplicationErrorHandler {

	public GenericResponse<Object> handleApplicationError(IApplicationError applicationException);
}
