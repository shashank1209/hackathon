package com.freecharge.hackathon.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.freecharge.hackathon.constants.Constants;
import com.freecharge.hackathon.exceptions.IApplicationError;
import com.freecharge.hackathon.exceptions.ResponseError;
import com.freecharge.hackathon.models.responses.GenericResponse;
import com.freecharge.hackathon.properties.UserMessageProperties;

@Component
@PropertySource(Constants.USER_MESSAGES_PROPERTY_FILE)
public class ApplicationErrorHandler implements IApplicationErrorHandler {

	@Autowired
	private UserMessageProperties userMessageProperties;

	@Override
	public GenericResponse<Object> handleApplicationError(IApplicationError applicationException) {
		GenericResponse<Object> response = new GenericResponse<Object>();
		response.setCode(applicationException.getError().getHttpStatus().value());
		response.setMessage(applicationException.getError().getHttpStatus().getReasonPhrase());
		ResponseError responseError = new ResponseError();
		responseError.setErrorCode(applicationException.getError().getErrorCode());
		responseError.setStatus(applicationException.getError().getStatus());
		String userMessage = applicationException.getUserMessage();
		if (StringUtils.isEmpty(userMessage)) {
			userMessage = userMessageProperties.getUserMessage(applicationException.getError().getErrorCode());
		}
		responseError.setUserMessage(userMessage);
		response.setErrors(responseError);
		return response;
	}
}
