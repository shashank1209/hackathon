package com.freecharge.hackathon.properties;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.freecharge.hackathon.constants.Constants;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "user")
@PropertySource(Constants.USER_MESSAGES_PROPERTY_FILE)
@Data
public class UserMessageProperties {

	private Map<String, String> messages = new HashMap<String, String>();

	public String getUserMessage(String errorCode) {
		if (messages.containsKey(errorCode)) {
			return messages.get(errorCode);
		}
		return messages.get(Constants.DEFAULT);
	}
}
